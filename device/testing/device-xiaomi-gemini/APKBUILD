# Maintainer: Raffaele Tranquillini (cunidev) <raffaele.tranquillini@gmail.com>
# Reference: <https://postmarketos.org/devicepkg>

pkgname=device-xiaomi-gemini
pkgdesc="Xiaomi Mi 5"
pkgver=1
pkgrel=4
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	linux-postmarketos-qcom-msm8996
	mesa-egl
	mkbootimg
	postmarketos-base
	postmarketos-update-kernel
	soc-qcom-msm8996
	soc-qcom-msm8996-ucm
	"
makedepends="devicepkg-dev"
subpackages="$pkgname-nonfree-firmware:nonfree_firmware $pkgname-phosh"

source="
	30-gpu-firmware.files
	deviceinfo
	gpu-nobin.sh
	pointercal
	phoc.ini
	"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname

	install -D -m644 "$srcdir"/pointercal \
		"$pkgdir"/etc/pointercal

	install -D -m644 "$srcdir"/gpu-nobin.sh \
		"$pkgdir"/etc/profile.d/gpu-nobin.sh
}

nonfree_firmware() {
	pkgdesc="GPU/Modem/Venus/ADSP/SLPI/WiFi/Bluetooth Firmware"
	depends="
		firmware-xiaomi-gemini
		linux-firmware-ath10k
		linux-firmware-qca
		soc-qcom-msm8996-nonfree-firmware
		"
	install="$subpkgname.post-install"
	mkdir "$subpkgdir"

	install -Dm644 "$srcdir/30-gpu-firmware.files" -t \
		"$subpkgdir/etc/postmarketos-mkinitfs/files"
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"
	install -Dm644 "$srcdir"/phoc.ini \
		"$subpkgdir"/etc/phosh/phoc.ini
}


sha512sums="
a1ca623314e80045ea8d64be06d12ffc091a5585ca40f9713039883e20889ba47f4a3f9c3bd92bd3b616bb6bb621e87409b6b6fa43bfe566a2a46468609c16f4  30-gpu-firmware.files
9309d4d5eadb8241f54c305702c6297939aa30d36bae3470f7ff62a3938c1c8c2c7d6cb76a51ab3d6d32c50d115c765880f6295cbb7cdb0933b6d40d0def6b0b  deviceinfo
ecd40b8f9bc17f383c52aa96ad23ceec41be75a4500d5e7a67f2f59875bf9f72eb35686a0b6e4949b45fd589c5da409d1212c52172fef94de2d1655b28c339b4  gpu-nobin.sh
e9ee9c1404b41ba15eafff3f728ff6e0e78dc17e9a88483fca92db1aa3efe0e4d5bf26142e533c4ff12b1bf8ce04ccabb7ca0f93aaea586d5b30910282aad237  pointercal
38409d4ce48db4624539dbfd695d3c6e1596a29fd7c3004a41368b28cba646495d05360420ad728f82c63d6ddeeca71d09944df06ebaca120aedd9cc2b436eb3  phoc.ini
"
